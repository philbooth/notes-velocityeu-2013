# Velocity Conf EU 2013

* [Wednesday](#wednesday)
    * [Addy Osmani / Gone in 60 frames per second](#addy-osmani--gone-in-60-frames-per-second)
    * [Yoav Weiss / Responsive image techniques & beyond](#yoav-weiss--responsive-image-techniques--beyond)
    * [Andy Davies & Tobias Baldauf / Hands-on web performance](#andy-davies--tobias-baldauf--hands-on-web-performance)
* [Thursday](#thursday)
    * [Ed Fletcher / Turning performance metrics into improved web & mobile experience](#ed-fletcher--turning-performance-metrics-into-improved-web--mobile-experience)
    * [Michael Klepikov / Developer-friendly web performance in continuous integration](#michael-klepikov--developer-friendly-web-performance-in-continuous-integration)
    * [Tammy Everts / Mobile web stress: understanding the neurological impact of poor performance](#tammy-everts--mobile-web-stress-understanding-the-neurological-impact-of-poor-performance)
    * [Joshua Hoffman / High velocity migration](#joshua-hoffman--high-velocity-migration)
    * [Ilya Grigorik / Breaking the 1000ms mobile barrier](#ilya-grigorik--breaking-the-1000ms-mobile-barrier)
* [Friday](#friday)
    * [Morning stuff](#morning-stuff)
    * [Mandy Waite & Marc Cohen / Provisioning the future - building and managing high performance compute clusters in the cloud](#mandy-waite--marc-cohen--provisioning-the-future---building-and-managing-high-performance-compute-clusters-in-the-cloud)
    * [Jonathan Klein / Upgrading the web - driving support for new standards](#jonathan-klein--upgrading-the-web---driving-support-for-new-standards)
    * [Rick Viscomi / Learning from the worst of WebPageTest](#rick-viscomi--learning-from-the-worst-of-webpagetest)
    * [Toufic Bounez / Beyond pretty charts... analytics for the cloud infrastructure](#toufic-bounez--beyond-pretty-charts-analytics-for-the-cloud-infrastructure)

## Wednesday

### Addy Osmani / Gone in 60 frames per second

* reads of clientXxxx, offsetXxxxx, scrollXxxxx, innerText all causes of jank (cache their values)
* drop shadow, blur, linear gradient, fixed background images (expensive styles)
* don't do work in scroll event handlers
* devtools > settings > show fps meter (adds frame rate meter to top right of rendered view)
* unnecessary paints: position:fixed, overflow:scroll, :hover effects, touch event listeners
* long paints: complex css, image decoding, large empty layers
* transform:translate|scale|rotate and opacity can be animated cheaply
* devtools > settings > show paint rectangles - highlights repaint areas
* devtools > settings > force accelerated compositing - options for composite layers
* translateZ/translate3d layer promotion hack has been disabled in ios
* use -webkit-perspective and -webkit-backface-visibility instead
* www.lexus.asia - really nice use of composite layers
* devtools layers view has cad-like model of different layers, lists compositing reasons too
* forced synchronous layout - browser being forced to reflow before it should
* Use FastDOM to batch DOM reads/writes (uses requestAnimationFrame to batch operations)
* look at paul irish's timeline cheatsheet
* use flame chart to analyse script performance issues
* paint slowness, turn on paint reactandles & layer borders options in devtools
* about:tracing - see matt deuchar's talk
* don't use margin-top or background-position for parallax scrolling, promote the layer instead with translate3d
* bootstrap 3 - optimised, performs much better than 2, see paul irish video
* pre-scale images in server for double win - bytes down the wire and time in browser paint
* if you need scrollTop, cache it in scroll event handler then use cached var in rAF handlers
* definitely profile stuff with red borders on
* position:fixed may trigger layer automatically in the future
* anti-aliasing text requires opaque layers
* consider applying -webkit-backface-visibility just for the duration of an animation rather than all the time
* consider disabling :hover effects for the duration of scroll events
* firefox: paint flash
* http://jankfree.org/
* console.timeline and console.timelineEnd use these from js to add events to devtools timeline

### Yoav Weiss / Responsive image techniques & beyond

* sizer soze - measures difference between actual image size and optimum display size, for diff viewports
* sizersoze.org - sizer soze as a service
* chrome has 32mb of cache for decoded images, once that is filled you pay to re-decode them
* usecases.responsiveimages.org - released as a wg note, documents use cases for reponsive images
* option 0: cookie based
* option 1: low quality image in src attribute, interrogate with script later, modify to appropriate img src
    * advantages:
        * (effect is like progressive jpg)
        * (can work without server-side logic)
        * (is progressively enhanced, so you still get low quality without js)
    * disadvantages:
        * double download
        * markup contains multiple resources
        * final quality image download starts late
* option 2: picturefill.js, uses <picture> markup pattern, markup for diff resource sizes with media queries in data attributes on spans, script then injects correct image, noscript is there for fallback image, best for art direction
    * advantages:
        * no double download
        * falls back gracefully
        * media querie based syntax is familiar
    * disadvantages:
        * if scripts run but fail, there is no fallback (script errors are much more common than script disabled)
        * verbose for viewport switching
        * markup contains multiple resources
        * image download starts late
        * verbose if many media queries
* option 3: x-picture, based on web components, <picture> polyfill, image downloaded on parse, requires polyfill
    * advantages: as 2
    * disaadvantages: as 2
* option 4: mobify.js, attempt to enable script access before preloader or parser, injects plaintext tag to DOM, stops doc being parsed as html, then downloads the rest of their script which reads html from the plaintext element, modifies src attributes, then document.open() and document.write() the modified markup for the browser to run, starting from scratch
    * advantages:
        * no markup changes
        * no double download
        * preloader works, after initial delay
        * if no js, everything reverts to normal
    * disadvantages:
        * blocking javascript download, execute
        * html is parsed in one go rather than sequentially as it arrives
        * compatibility, doesn't work in oldIE
        * too invasive for old devices
        * if js fails after plaintext injection, you get a blank page
* future - service worker api, offline api, enables URL rewriting in the client
    * being implemented in ff & chrome
    * doesn't work on first load because of requirement on rewrite script
* compressive images
    * no markup, no js
    * compress 2x images to LQ0, let browser handle resize
    * looks good on retina, byte size is no bigger than 1x
    * leaves burden for decoding and resizing on the client
    * not perfect
    * only works up to 2x
* bandwidth testing
    * eg foresight.js
    * tests with connection xhr, then downloads actual image
    * blocks downloading, pays for an extra useless resource
* passive bandwidth testing
    * use navigation timing api, draw conclusions from that about what resources to fetch
    * not aware of anyone using this approach
    * bandwidth varies, not a good metric
* lots of tooling around to automate image resizing
    * services, build tools, cms plugins
* if you can't modify page, you must use a compressive image solution
* if you can modify head, js solutions come into play
* if you can modify the image element, those solutions come into play
* if you have server side control, more options
* preloader understands noscript, won't download stuff in there if script is enabled
* src trade-off: using it ensures double download, not using it ensures no fallback
* prefer the solution that downloads images as soon as possible
* coming standards (client-side): srcset, picture, src-N
* coming standards (server-side): content negotiation
* responsive image container: coming file format approach, layered images with different resolutions

### Andy Davies & Tobias Baldauf / Hands-on web performance

* webapgetest now lets you compare waterfalls
* cloudshark, hosted wireshark, you can load tcpdumps from webpagetest there
* webpagetest api wrapper: swiss army knife for webpagetest, runs on node, integrates with Jenkins via TAP (need to be running webpagetest on your own server)
* recommend running webpagetest on your own machine
* robotest agent - runs on mobile device, connects to your webpagetest process, logs results there
* sitespeed.io - uses phantomjs, lets you impersonate user agents and resolutions, then you can run tests against it
* httparchive - 300,000 URLs - can set up your own copy
* r - statistical language, good for data analysis, can import data from httparchive
* rstudio - makes working with r much easier
* consider keeping css on the same domain rather than cdn
    * browser has opened a second connection to web server immediately anyway
    * the cdn connection may be delayed for dns lookup etc
* always pick an odd number of tests in webpage test so you are looking at the median and one either side
* google serve woff files from different domain to the original font request, which can delay
* chrome preloader gives higher priority to javascript, which can delay images further up the page if there is lots of it at the bottom of the body
* firefox is more nuanced than that
* "enterprise cms' are reassuringly expensive" i.e. ttfb always suffers
* css checkbox hack

## Thursday

### Ed Fletcher / Turning performance metrics into improved web & mobile experience

* Use KeyNote
* Beat developers with a stick
* Why did I come to this talk?

### Michael Klepikov / Developer-friendly web performance in continuous integration

* Current performance tools are difficult to integrate with CI, often bound to browser etc
* Performance can be difficult to fix in production
* Performance test can be pront to rot
* Selenium 2.0 web driver is dominant front-end testing tool, has nothing for performance testing out of the box
* RUM is more important than performance tests
* Use devtools trace to capture metrics in Selenium tests
* Run many iterations, analyse data statistically
* Don't use webpagetest to record results, use it as a UI and just send it results that you've collected
* TSViewDB for graphing and showing performance regressions in webpagetest
* Summary:
    * Use functional tests as your base
    * collect RUM data
    * run continuously
    * autodetect regressions
    * send regressions to author of offending changes
    * need to debug production much less
    * frees ops staff to work on interesting problems

### Tammy Everts / Mobile web stress: understanding the neurological impact of poor performance

* 2010 foviance study hooked people up to eegs while they used the web
* subjects found it 50% harder to concnetrate when connection was throttled
* desktop usage reducing 1% per month
* radware did some research about web stress for mobile users
* 2/3 mobile users expect websites to load in under 4 seconds
* most sites don't meet that target
* median mobile load time is 7.84 seconds in top 100 sites on mobile
* 2 sites loaded in under 4 seconds
* by 2017, mobile ecommerce will be 26% of all ecommerce (16% currently)
* abandonment of shoping carts is 39% greater on mobile than on desktop
* 95% of a consumer's decision are made at the subconscious level
* research shows that patients with damage to emotional area of the brain can't make decisions
* surveys are a bad form of research for consumer behaviour then, because they occur at a post-cognitive level
* benefits of neuro-scientific testing:
    * evaluates think/feel, not say
    * quantified data
    * moment-by-moment interaction
    * ?
    * cause & effect
* radware emotional eeg study
    * tested john lewis, tesco, easyjet, ryanair
    * 20 testers, both genders, pre-screened (not mental), familiar with mobile device usage, didn't know they were oart of a performance study
    * small sample size is okay for usability studies because you're finding *any* problems not looking for statistical significance
    * gave testers a standard set of shopping tasks on each site
    * served one of two sites, normal wifi or with 500ms delay
    * used an eeg heaadset and eye tracking so that eeg readings could be linked to what they were looking at
    * 500ms delay was used because it's been shown to be the trigger point for bounce rate, conversion rate etc to deteriorate
    * user frustration and emotional engagement were most affected by the 500ms delay
    * normal page load times were 3-ish seconds
    * increase in frustration with delayed loads, ranged from 10% to 30%
    * decrease in emotional engagement wth delayed loads, ranged from 3% to 6%
    * exit interviews were conducted with the testers, adjectives were fed into tag clouds
    * dominat term by a huge margin for normal sites was 'easy to use'
    * lots of words for the delayed sites: 'boring', 'clunky', 'slow', 'basic', 'tacky', 'sluggish', 'inelegant', 'confusing', 'frustrating'
    * not all about the slowness some are about design/brand
    * different sites triggered different results at different points (browse/checkout)
    * also did an implicit response test on subjects
        * people react faster to congruent stimuli than incongruent (e.g. colour written in the same colour or not)
            * project implicit - harvard thing that tests this
        * before the test, easyjet and ryanair were used on the subjects, testing reaction speed to seeing logo after being primed with emotive terms
        * results of pre-test were that easyjet was ahead on most areas, ryanair better on purchase intent
            * trusted, easy, value, aappealing, impressive - some of the areas easyjet was ahead
        * my mind wandered while she discussed post-test, whoops
* obamacare is a current example of how people react to slow websites - obama himself tarnished by the episode
* takeaways
    * mobile users significantly affected by slow performance (up to 26% increaase in frustration, up to 8% decrease in engagement)
    * slow sites can seriously undermine overall brand health
    * nature and scale of the impact varies
    * ...
    * ... she skipped the last 2 points unfortunately (out of time)
* THIS TALK WAS A BLOODY TRIUMPH!

### Joshua Hoffman / High velocity migration

* keys for growth:
    * automated provisioning
    * config management
    * comprehensive monitoring & alerting
    * deployment tools
* tips for surviving insane growth:
    * pay down technical debt
    * need heroes to stay up all night paying down technical debt
    * kill projects that have blown scope and suck up resources
    * take shortcuts
    * prefer refactoring to green-field development
* 3 questions for any project:
    * what problem does it solve?
    * what are the specific benefits of this solution?
    * what are the specific costs of this solution?

### Ilya Grigorik / Breaking the 1000ms mobile barrier

* aka waging war on latency and optimising the critical rendering path (crp)
* RTT = round trip time
* 1000ms is significant because it is the delay before users context switch mentally, before then they're still with your app
    * 0 - 100 ms = feels instant
    * 100 - 300 ms = feels sluggish
    * 300 - 1000 ms = machine is working...
    * 1000 ms = context switch
    * 10000 ms = user leaves to do something else
* hardware input latency takes 55-120 ms to register a touch event on mobile device
* decode to specific touch event (tap, double tap) takes 300 ms because that is the delay for deciding between the two (there is a jslib called fastclick that can help) (delay doesn't occur on chrome if meta[@name=viewport]/@content is width:device-width
* control-plane latency then kicks in to fire up the radio on the device (< 100 ms for LTE and HSPA, < 2.5s for 3g)
* mobile carrier network introduces more latency (40-50ms LTE, 50-200ms HSPA+, 150-400ms HSPA, 600-750ms EDGE, ????)
* then dns lookup, tcp connection, tls handshake, http request, server processing time add more latency
* total latency at this point is 1055 ms - 4300 ms in 3G or 555 ms - 1220 ms in 4G
* to break the 1000 ms mobile barrier you:
    * must optimise network scheduling & efficiency
    * must optimise critical rendering path in browser
    * must predict, pre-resolve, pre-fetch, pre-render
* to optimise network efficiency:
    * place bits closser to user - replace rtt, use cdn
    * ship fewer bits - compress everything, elimate unnecessary code
    * reuse connections - 1 rtt for handshake, slow-start
    * optimise tls - session resumption, record size
    * eliminate unnecessary latency - redirects, dns lookups
* High Performance Browser NEtworking - Ilya's book, O'Reilly published
* critical rendering path - order of document affects how quickly the browser can render the page
    * screen can't be painted until css has been parsed even though the dom is ready
    * js can read/write cssom and read/write dom
    * js will block on css
    * js can block DOM construction
    * outstanding css blocks js execution
* eliminate blocking js
* load analytics, twitter, maps etc after load event
* pagespeed insights - can put a URL in and get a report about page speed - LOOKS AWESOME!
* pagespeed insights also available as chrome extension
* start thinking in RTTs
* build an RTT view of critical rendering path, identify blocking resources holding back the first render
* critical rendering path is how many RTTs it taks to get visible stuff on the screen
* render above the fold content first - browser will do this if you give it the right infomation
* one rtt render for above the fold content
* no redirects + sub-200ms server response
* optimise critical rendering path - inline critical css, minimise and postpone js
* count and eliminate unnecessary roundstrips
* defer all non-critical assets
* condsider inlining above the fold css and js (keep it lightweight, less than 14kb)
* progressive enhancement usually means you can defer js - another good reason for progressive enhancement
* paint is blocked until all the css has arrived
* if you can async non-critical css, you will improve load times
* use mod_pagespeed (apache) and ngx_pagespeed (nginx) they do css async automagically by detecting which rules are above the fold and then inlining those styles and deferring the others - THIS WORKS!!!
* you've got the pixels to screen quickly, are those pixels useful pixels?
* avoid plugins (flash etc)
* configure layout viewport
* fit contents to viewport width
* make text legible
* size tap targets appropriately
* HN on mobile is perfect anti-example for these
* pagespeed insights is ruddy marvellous
* use link[rel = dns-prefetch/prefetch/prerender]
    * dns-prefetch pre resolves hostnames for assets later in the page (most browsers)
    * prefetch prefetches assets for future navigation(most browsers)
    * prerender prerenders page in background for future navigation
* google have increased tcp congestion window, requires linux kernel upgrade on server

## Friday

### Morning stuff

* Image sizes growing 30% per year (source: httparchive)
* wpt-script - automation for webpagetest (on github)
* soasta.com/mpulseux - soasta's rum equivalent of httparchive
* boomerang.js - rum fallback for browsers that don't support navigation timing api
* devtools > audit > web page performace - shows how many style rules are unused in current page
* grunt-uncss - plugin for removing unused css rules
* uncss - the core module underneath the above
* Fucking grunt everywhere ffs

### Mandy Waite & Marc Cohen / Provisioning the future - building and managing high performance compute clusters in the cloud

* Compute Engine - Google's AWS competitor
* charged at 1 minute granularity, 10 minute minimum
* persistent disk - storage product, split across multiple volumes, multiple copies of data, HMAC checksums for integrity, heavily optimised
* network set up supports routing, load balancing
* everything published via api, lots of thrid-party tools, puppet & chef support
* cluster management case study
    * requirements:
        * use clusters and vms
        * use open source software only
        * make cluster self-configuring
        * be independent of life-cycle management tool and computing environment (not just compute engine)
    * using zeromq to distribute requests between master and slaves
        * zeromq is awesome, server side pub-sub for distributed cluster of listeners
    * because of that architecture, new nodes that are spub up later will just join in and be available to work, no other set-up needed
    * ditto for nodes leaving the cluster
* persistent disk is configured to be consistent - load times, access times etc should vary very little
    * works with ricghtscale, scalr, chef, puppet, vagrant, ansible, support for salt & docker coming
* google actively working with 3rd parties to keep compute engine running well with open source community
* vagrant-google is the vagrant extension (on github)
* persistent disks are locked to a zone, but snapshots can be used anywhere, allowing cross-pollination of disks between different data centers/zones
* best practices:
    * automate everything, manage configuration like code
    * prepare for maintenace events and outages, test outages
    * reliable systems = infrastructure + design + people
    * design self-configuring vms and clusters
    * use managed services (eg google cloud storage)
* new features in compute engine:
    * transparent scheduled maintenance, take zones down partially, automatically migrate affected apps to unaffected parts of the zone (not in eurpoe until 2014)
* load balancing, ingrained in the infrastructure, client affinity means ip addresses always routed to same instance, backup pool used if instances become unavailable / unhealthy
    * differential persistent disk snapshots, snapshots are billed by space used, 1st snapshot is full, subsequent snapshots are deltas to reduce cost
* http://cloud.google.com/
* $2,000 credit - 'mwa-in' promo code at http://cloud.google.com/starterpack ($1k of app credits, $1k compute credits)

### Jonathan Klein / Upgrading the web - driving support for new standards

* speeed index - smaller is better, measures area above the load in graph, effectively the visual appearance of loading time
* http origin / hop hints
    * ietf draft
    * http response header
    * tells the browser about server capabilities
    * supports:
        * small request headers
        * relative referers
        * omitting cookies
        * sharing connections
        * pipeline depth
    * aim is to reduce heaader sizes and optimise transfer
    * no content changes required, configuration change only
    * not implemented yet
* client hints
    * ietf draft
    * http request header
    * tells server about browser capabilities
    * allows you to vary response based on client (eg send webp images where supported)
    * simplifies markup, cleaner than picturefill etc
    * only implemented in chrome canary right now
* webp
    * new image format from google
    * 25%-34% smaller than jpeg
    * has lossless option
    * supports alpha transparency
    * growing adoption
    * supported in chrome, opera and android browser
    * takes a bit longer to decode than jpg
    * uncertain future, no firefox support
    * no progressive decoding yet, it is planned for a future spec
* spdy / http 2.0
    * spdy - google protocol that is an early implementation of http 2.0, will go away once http 2.0 is finalised
    * supports http multiplexing, header compression, prioritisation
    * widely supported - firefox, chrome, android, opera, ie 11+
    * no safari support
    * no more domain sharding necessary with spdy, it is an anti-pattern now
    * no more image sprites
    * no more need to concatenate assets
    * no more data URIs, spdy has a server push capability so no need to inline content any more
    * ilya's book is good on this (high performance browser ????, o'reilly)
    * connection view in webpage test shows difference
    * before spdy, 6 connections to server, things back up
    * after spdy, 1 connection to sserver, things don't back up
    * no change to bytes sent
    * start render occurs quicker (~5%)
    * speed index is quicker (~15%)
    * uncontroversial among browser vendors
    * in production today - all google domains use it, facebook, twitter, most of the major https sites use spdy
    * forces you to serve your site securely (SSL/TLS)
* prefetching assets
    * `<link rel="prefetch" href="..."/>`
    * assets are sent in one request
    * reduces bytes down the wire
    * improves speed index
    * reduces number of requests
    * not implemented in chrome yet (impl taken out as buggy) will be there soon
    * already in firefox
* webp, spdy and prefetch are ready to be used right now
* convincing cdns to use spdy is a big challenge, needs cdn implementation to be really effective
* implement as much as you can, speak/blog about experiences with them (problems & successes), ask cdns and browser to support, report bugs

### Rick Viscomi / Learning from the worst of WebPageTest

* waterfall slope - vertical good, horizontal bad
* antipatterns:
    * cancelled requests - caused by preloader with charset/x-ua-compatible/base href being set too late, so preloader has to cancel its requests
        * that is why response headers trump meta tags
        * avoid base element
    * slow scripts - locks ui, delays paint
    * poor median selection, load event can be a poor metric, some sites ready before load, others not ready until well after it
* configuration:
    * base tests on real user journeys taken from analytics
    * better to focus on perceived load than load event
    * use the marker function in webpagetest api to get events to show up in webpagetest, use that to gauge perceived load
* contribution:
    * source for webpagetest is on github, read the source, raise issues, submit PRs
* https://github.com/rviscomi/webpagetest
    * supports multivariant testing (locations, urls, browsers) in webpagetest (running on your own machine/server)
* use title templates to give the test results meaning - {url}, {browser} etc

### Toufic Bounez / Beyond pretty charts... analytics for the cloud infrastructure

* you might be more interested in a rapid increase from 60% than stable at 89% - 91%
* anomaly detection / outlier detection
* current monitoring tools assume system is relatively static
* good for detecting catastrophic events, not much else
* assume a normal distribution
* run histogram on data before analysis to know distribution curve
* with normal dist, 68% values are within 1 sd, 95% within 2 sd's, 99.73% within 3 sd's
* this is where assumptions for standard monitoring tools come from
* seasonality, spike influence, normality, parameters - four horseman of modelpocalypse (not captured by normal distribution based checks)
* mean is bad, moving averages are better - use a finite window of past values, pick a new mean and use that as moving average
* simple moving average - average of last n values, all values in window contribute equally, is affected by spikes and outliers
* weighted moving average - linearly decreasing weights to every value in the window, older values contribute less to the prediction
* exponential smoothing, improves wma with exponentially decreasing weights
* double smoothing, triple smoothing, adds trend smoothing factor, does not deal with seasonality
* TES, Holt-winners introduces seasonality
* all of the above assume normal / gaussian distribution
* when distribution is not normal, these metrics are triggered way too often and don't work
* many non-gaussian techniques which can avoid this
* kolmogorov-smirnov test - compare two probability distributions, measures maximum distance between cumulative distributions, can be use to compare periodic/seasonal data (compare week to week or day to day and so on), much better at alerting for non-gaussian distributions
* lots of monitoring data is non gaussian, use appropriate techniques for your distribution
* don't need to collect all data, just need enough of a sample to see patterns
* ability to collect data is not reason to actually collect it
* equally, don't over abstract the data
* you don't need more resolution than twice you highest frequency (nyquist-shannon sampling theorem)
* watch out for correlated metrics (eg used vs available memory, no need to collect both)
* norvig.com/chomsky.html - debate between norvig and chomsky
* kale
* get distribution by running a histogram or a model fitting... loads of open source tools
* powerlog? approach to alerting - don't alert on the first thing, alert when issues become persistent

